import { ModuleWithProviders} from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { ApplicationComponent } from '../body/application/application.component'
import { DetailsItemComponent } from '../body/detailsItem/details.item.component'

const appRoutes: Routes = [


	{ path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', 		component: ApplicationComponent },
    { path: 'details/:id', 		component: DetailsItemComponent },
	


];

export const appRoutingProviders: any[] = [];
export const routing:ModuleWithProviders = RouterModule.forRoot(appRoutes);
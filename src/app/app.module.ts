import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { ApplicationComponent } from './body/application/application.component';
import { SimpleItemComponent } from './body/simpleItem/simple.item.component'
import { DetailsItemComponent } from './body/detailsItem/details.item.component'

//Services
import {ServiceComponent} from './services/service.component'

//Pipes
import  { PipeLimitText} from './pipes/pipe.component'

//Router
import { RouterModule, Routes } from '@angular/router';
import { routing, appRoutingProviders } from './router/router.component';

//Animation
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
  declarations: [
    AppComponent,
    ApplicationComponent,
    SimpleItemComponent,
    DetailsItemComponent,
    PipeLimitText
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    routing,
    BrowserAnimationsModule
  ],
  providers: [
    ServiceComponent,
    appRoutingProviders 
  ],
  exports: [
		RouterModule
	],
  bootstrap: [AppComponent]
})
export class AppModule { }

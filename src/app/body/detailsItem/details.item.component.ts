import { Component, Input, OnInit   } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceComponent } from '../../services/service.component'

@Component({
  selector: 'app-details-item',
  templateUrl: './details.item.component.html',
  styleUrls: ['./details.item.component.css']
})

export class DetailsItemComponent {

  public item:any;

  constructor(private _route: ActivatedRoute, private _service:ServiceComponent){

  }

  ngOnInit() {
    this._route.params.subscribe(params => {
       this.item = this._service.getElementById(params.id)
    });
  }

  selectImage(img){
    return img == "" ? "assets/no_download.png" : img;
  }

     
  toHTML(input) : any {
    return new DOMParser().parseFromString(input, "text/html").documentElement.textContent; 
  }
}

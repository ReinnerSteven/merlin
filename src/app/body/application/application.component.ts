import { Component, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import { ServiceComponent } from '../../services/service.component'
import { Router} from "@angular/router";
declare var $: any;

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css'],
  animations: [ trigger('showAnimation',[
    state('start',style({
      opacity:0,
      

    })),
    state('end',style({
      opacity:1,
      

    })),
    transition('start => end',animate(1500)),//stados para la transicion
    transition('end => start',animate(1500)),
  ])]
})
export class ApplicationComponent {
  //[@showAnimation.start] = 'cabal()'

  public stateAnimation = 'start' // [@showAnimation]
  public currentItem;
  public currentCagory = "Todos"
  public listElements = [];
  public listCategories = [];
  public listFilterElementes = [];
  

  constructor(public _service:ServiceComponent, private _router:Router){

  }

  ngOnInit() {
      //Make a request
  
    this._service.getItems().subscribe(
      data => {
          
        this.listElements = data["data"].children;        
        this._service.insertElements(this.listElements);
        localStorage.setItem('listElements', JSON.stringify(data["data"].children));
        this.createCategories();
        this.listCategories.push("Todos")
        this.filterElementesByName(this.listCategories[0]);
        this.currentCagory = this.listCategories[0];
        this.stateAnimation = 'end'
      },
      err => {
        console.log("Error occured.",err)
        this.listElements = JSON.parse(localStorage.getItem("listElements"));
        this._service.insertElements(this.listElements);
        this.createCategories();
        this.listCategories.push("Todos")
        this.filterElementesByName(this.listCategories[0]);
        this.stateAnimation = 'end'
      }
    );
  }

  createCategories(){
    const self = this;
    this.listElements.forEach(function(element){
      element.data.advertiser_category = element.data.advertiser_category == null ? "Otro": element.data.advertiser_category
      self.listCategories.push(element.data.advertiser_category );
    })
    this.listCategories = this.removeDuplicateUsingFilter(this.listCategories);
  }

  removeDuplicateUsingFilter(arr){
    let unique_array = arr.filter(function(elem, index, self) {
      return index == self.indexOf(elem);
    });
    return unique_array;
  }

  changeText(event){

    var value = event.srcElement.value
    if(value == "" || value == " "){
      this.filterElementesByName(this.listCategories[0]);
      return 
    }
    this.filterByTitle(value)
  }

  filterByTitle(text){
    this.listFilterElementes = this.listElements.filter(function(item,index){
      if(item.data.header_title == null) 
        return false

      var title = item.data.header_title.toLowerCase()
      return title.indexOf(text.toLowerCase()) >= 0 ;
    })
  }
  
  filterElementesByName(name){
    
    if(name=="Todos"){
      this.listFilterElementes =  this.listElements;
      return
    }
      

    this.listFilterElementes = this.listElements.filter(function(item,index){            
      return item.data.advertiser_category == name ;
    })       
    
    this.currentCagory = name;
  }

  selectedCurrentItem(item, index){
    const url = 'details/'+item.data.id
    this._router.navigate([url]); 
    $('html, body').animate({
      scrollTop: $("#router-app").offset().top
  }, 1000);
  }
}

import { Component, Input, Output, EventEmitter   } from '@angular/core';

@Component({
  selector: 'app-simple-item',
  templateUrl: './simple.item.component.html',
  styleUrls: ['./simple.item.component.css']
})
export class SimpleItemComponent {

    @Input() item: any;
    @Output() clickDetails = new EventEmitter<any>();



    constructor(){ }

    selectImage(img){
      return img == "" ? "assets/no_download.png" : img;
    }

    showDetails(item){
      this.clickDetails.next(item);
    }

    toHTML(input) : any {
      return new DOMParser().parseFromString(input, "text/html").documentElement.textContent; 
    }
}

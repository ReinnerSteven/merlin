import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name:'textPipe'})

export class PipeLimitText  implements PipeTransform {
    transform(str:string):string{
        return str.length > 150 ? str.substring(0, 150) + '...' : str; 
    }
}
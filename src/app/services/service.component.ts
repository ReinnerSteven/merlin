
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class ServiceComponent {

  private api = "https://www.reddit.com/reddits.json";
  
  private listElements = [];

  constructor(private _http: HttpClient){

    this.listElements = JSON.parse(localStorage.getItem("listElements"));

  }

  getItems(){
    return this._http.get(this.api);
  }

  insertElements(list){
    this.listElements = list
    console.log("insert",this.listElements)
  }

  getElementById(id){
    
    var itemReturn = null;
    this.listElements.forEach(function(element){    
      if(element.data.id==id){
        itemReturn =  element
      }
    })
    return itemReturn;
  }


}